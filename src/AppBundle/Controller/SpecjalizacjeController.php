namespace AppBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;

class SpecjalizacjeController extends Controller
{
    public function viewSpecjalizacje()
    {
        
        return $this->render(
            'full/specjalizacje.html.twig'
           
        );
    }
}