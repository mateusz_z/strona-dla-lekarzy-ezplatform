<?php

namespace AppBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;

class AktualnoscController extends Controller
{
    /**
     * 
     *
     * @param ContentView $view
     *
     * @return ContentView $view
     */
    public function viewAktualnosc(ContentView $view)
    {
        $repository = $this->getRepository();
        $contentService = $repository->getContentService();
        $currentContent = $view->getContent();

        return $view;
    }
}
